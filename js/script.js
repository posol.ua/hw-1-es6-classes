/* Теоретичне питання
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
Прототипне наслідування в JavaScript – це спосіб організації об'єктно-орієнтованого програмування, де об'єкти можуть успадковувати властивості 
та методи від інших об'єктів.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
Ключове слово super() нам потрібно для виклику або перенесення методів батьківського класу в конструктор нового класу нащадка. 
Можна сказати відтворення методів батьківського класу для подальшої взаємодії з методами нащадка.

Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.*/

class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        return this._name;
    }
    set name (newName) {
        this._name = newName;
    }
    get age () {
        return this._age;
    }
    set age (newAge) {
        this._age = newAge;
    }
    get salary () {
        return this._salary;
    }
    set salary (newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary * 3;
    }
    get lang () {
        return this._lang;
    }
    set lang (newLang) {
        this._lang = newLang;
    }
}

const programmer1 = new Programmer ('Serhii', 37, 3000, 'Javascript');
console.log(programmer1);
console.log(`Employee is Name: ${programmer1.name}, Age: ${programmer1.age}, Salary: ${programmer1.salary}, Languages: ${programmer1.lang}`);

const programmer2 = new Programmer ('Alina', 25, 4000, 'Python');
console.log(programmer2);
console.log(`Employee is Name: ${programmer2.name}, Age: ${programmer2.age}, Salary: ${programmer2.salary}, Languages: ${programmer2.lang}`);

const programmer3 = new Programmer ('Ihor', 35, 5000, 'Java');
console.log(programmer3);
console.log(`Employee is Name: ${programmer3.name}, Age: ${programmer3.age}, Salary: ${programmer3.salary}, Languages: ${programmer3.lang}`);